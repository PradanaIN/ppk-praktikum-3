# PPK-Praktikum 3 : Web Service berbasis SOAP dan WSDL

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Deskripsi

Web service adalah suatu metode komunikasi antar dua perangkat elektronik lewat world wide web (www). Web service membantu kita dalam mengkonversi aplikasi kita menjadi aplikasi berbasis web. Saat ini terdapat 2 macam web service yaitu web service yang menggunakan SOAP dan WSDL, dan web service yang menggunakan REST, atau lebih dikenal dengan RESTful web service. Pada praktikum kali ini kita akan mempraktekkan pembuatan webservice berbasis SOAP dan WSDL. 

## Kegiatan Praktikum
## 1. Halaman WebService
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-3/-/raw/main/Screenshot/Screenshot%20(409).png)
## 2. Halaman WebService WSDL
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-3/-/raw/main/Screenshot/Screenshot%20(410).png)
## 3. Aplikasi Client CalculatorWSClient berhasil memanggil method add Web Service CalculatorWSApplication
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-3/-/raw/main/Screenshot/Screenshot%20(411).png)
